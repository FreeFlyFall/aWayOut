﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;

public class Patrol : MonoBehaviour
{
    public static Vector3 intersection;

    public GameObject player;
    public Transform head;
    public Transform[] patrolPoints;
    NavMeshAgent agent;
    public bool isPatrolling = true;
    public bool isLooking;
    public bool isChasing;
    public bool isSearching;
    public static bool isPlayerInSamePath; // Player is between intersections after losing los
    public Vector3 direction;
    public LayerMask layermask;
    public float waitTimer;
    public bool isWaiting;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        // Direction to center of player
        direction = player.transform.position - transform.position - transform.up;
        // FOV angle from enemy head
        float angle = Vector3.Angle(direction, head.up);

        // If the player is visible and is close, or is visible, in the field of view, and in sight range
        if ((Vector3.Distance(player.transform.position, transform.position) < 3 && losToPlayer() == true) ||
            ((Vector3.Distance(player.transform.position, transform.position) < 1000 && angle < 90) &&
            losToPlayer() == true))
        {
            // The player is in the same path between intersection and is being chased
            // Go to the last seen position if los is lost
            isPatrolling = false; isSearching = false; isLooking = false; isPlayerInSamePath = true;
            agent.destination = player.transform.position;
            isChasing = true;
        }
        // If the player is not seen, but the AI gets to lkp, and the player is in the same
        // path, chase the player
        else if (isChasing == true && agent.remainingDistance <= 0.25 && isPlayerInSamePath == true)
        {
            Debug.Log("Got to lkp, going to player");
            agent.destination = player.transform.position;
        }
        // If the player is not seen, but the AI gets the lkp, and the player passed through
        // an intersection, go to the intersection 
        else if (isChasing == true && agent.remainingDistance <= 0.25 && isPlayerInSamePath == false)
        {
            agent.destination = intersection;
            Debug.DrawLine(player.transform.position, intersection); // Flickers once
            isChasing = false;
            isSearching = true;
            waitTimer = 0;
        }
        //// Upon getting to intersection, look at the player no matter where they're at to attempt
        // to establish line of sight.
        else if (isSearching == true && isPlayerInSamePath == false && agent.remainingDistance <= 0) // Don't think I need the 2nd one
        {
            waitTimer += Time.deltaTime;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), 0.1f);
            Debug.Log("Waiting sec");
            //StartCoroutine(wait()); // for anim
            if(waitTimer >= 2)
            {
                isSearching = false;
            }
        }
        // Patrol if the player is not seen
        else if (agent.remainingDistance <= 0.15 && isWaiting == false)
        {
            Debug.Log("Setting new patrol point");
            isWaiting = true;
            StartCoroutine(wait());
        }
    }
    IEnumerator wait()
    {
        Debug.Log("waiting from coroutine");
        yield return new WaitForSeconds(2);
        int randomIndex = Random.Range(0, patrolPoints.Length - 1);
        agent.destination = patrolPoints[randomIndex].transform.position;
        Debug.Log("Going to next point");
        isWaiting = false;

    }

    bool losToPlayer() // Raycast check los to player from this gameobject
    {
        //Debug.DrawRay(transform.position + transform.up, direction, Color.cyan);
        RaycastHit hit;
        Physics.Raycast(transform.position + transform.up, direction, out hit, Mathf.Infinity, layermask);
        //Debug.Log("hit: " + hit.transform.name);
        Debug.DrawLine(transform.position + transform.up, agent.destination);
        if (hit.transform.tag == "Player")
        {
            return true;
        }
        return false;
    }
    // Implement chase behavior
    // Follow player until losing los, then move to the place where the player was last seen.
    // At any point, if the player is seen, chase the player again.
    // Implement search behavior
    // Add field of view to agent.
    // If the player is not seen, and the agent arrives at the place where the player was last
    // seen, determine whether the player has passed through an intersection. If they have, 
    // Go to that intersection and look around. If the player hasn't passed through an intersection,
    // set the player as the destination. If they player is seen at any point, chase the player.

    // The agent shouldn't turn around and go back down the path it came from after losing the player
    // and coming to an intersection. When the agent gets to an intersection, record the direction the
    // agent is facing. Then, according to this direction, exclude patrol points from being chosen
    // for the next destination which were behind the agent at the time of getting to the patrol point.
    // This can be done with coordinates. Determine N,S,E, or W heading for the agent at the time of
    // getting to the point. If facing N, the next point's y value must be greater than that patrol
    // point's. If facing E, the next point's x value must be greater than that patrol point's, etc.

    ///////////////////////////////
    // Add detection based on distance from player if there is los.
    // Add detection bias based on player status, e.g., crouching, sprinting, within los only.
}
